# Eggdev Server
Server app & dashboard for Project Material using Parse.

## [Unreleased]

## [0.1.0] - 2016-05-24
- Basic Egg management

## [0.0.0] - 2016-07-21
### Added
- Project initialization

[0.0.0]: https://bitbucket.org/dadrimon2-admin/postsingson-cms/branches/compare/v0.1%0Ddevelop#commits