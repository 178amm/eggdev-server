:: Start server
start "" parse-server "./config/env/development/parse-server-config.json"
:: Wait 2 seconds , give some time the server to launch
ping 1.0.0.0 -n 1 -w 2000 >NUL
:: Run dashboard
start "" parse-dashboard --config "./config/env/development/parse-dashboard-config.json"
