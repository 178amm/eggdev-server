# Eggdev Server
Server app & dashboard for Project Material using Parse.

## Installation 

* Install Parse Server globally using 
```
npm install -g parse-server mongodb-runner
```

* Install Parse Server Dashboard globally using 
```
npm install -g parse-dashboard
```

## Running 
* Start mongodb server
* Start parse server running
```
parse-server ./config/env/selectedenviroment/parse-server-config.json
```
* Start parse dashboard running
```
parse-dashboard --config ./config/env/selectedenviroment/parse-dashboard-config.json
```

## WARNINGS 
Cloud code path must be ABSOLUTE 

